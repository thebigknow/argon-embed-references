# frozen_string_literal: true

require 'json'
require 'openssl'
require 'base64'

if ARGV[0].nil? || ARGV[0].empty?
  STDERR.puts "You must pass the Base64 encoded string as an argument."
  exit(1)
end

client_id = "your_provided_client_id"
secret = "your_provided_client_secret"

user_info = JSON.parse(Base64.urlsafe_decode64 ARGV[0])
raise StandardError, "ClientId mismatch" unless user_info['client_id'] == client_id
data = Base64.decode64 user_info['token']
salt = data[0..15]
iv = data[16..27]
encrypted = data[28..-17]
auth_tag = data[-16..]

puts "SALT: #{Base64.strict_encode64(salt)}"
puts "IV: #{Base64.strict_encode64(iv)}"

cipher = OpenSSL::Cipher.new("aes-128-gcm")
cipher.decrypt
cipher.key = OpenSSL::KDF.pbkdf2_hmac secret, salt: salt, length: 16, iterations: 10_000, hash: "SHA256"
cipher.iv = iv
cipher.auth_tag = auth_tag
plain = cipher.update(encrypted) + cipher.final
#
puts plain
