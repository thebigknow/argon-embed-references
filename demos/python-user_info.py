import time
import base64
import hashlib
import json
# This must be the PyCryptodome library. PyCrypto does not support GCM
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

user_info = {
    "timestamp": int(time.time()),
    "user": {
        "identifier": 'some-client-owned-user-id',
        "first_name": 'Example',
        "last_name": 'User',
        "email": 'example.user@test.local'
    }
}

client_id = "your_provided_client_id"
secret = b"your_provided_client_secret"
salt = get_random_bytes(16)
key_len = 16
iterations = 10000
key = hashlib.pbkdf2_hmac('sha256', secret, salt, iterations, key_len)

iv = get_random_bytes(12)
cipher = AES.new(key, AES.MODE_GCM, iv)


etxt, auth_tag = cipher.encrypt_and_digest(
    json.dumps(user_info).encode())

token = base64.b64encode(salt + iv + etxt + auth_tag).decode()
user_info_param = {
    "client_id": client_id,
    "token": token
}

print(base64.urlsafe_b64encode(json.dumps(user_info_param).encode()).decode())
