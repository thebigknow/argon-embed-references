/* This is a file from a larger library that targets The Big Know's Public API
 * You can find the library in its entirety here:
 * https://bitbucket.org/thebigknow/argon-public-api-kotlin/src/develop/
*/
package fyi.tbk.argon.utils

import com.squareup.moshi.Moshi
import fyi.tbk.argon.api.models.User
import java.time.Instant
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
import kotlin.random.Random

class UserInfoCreator(private val clientId: String, private val secret: String) {
    private val salt: ByteArray = Random.nextBytes(16)
    private val iv: ByteArray = Random.nextBytes(12)

    fun encrypt(plaintext: String): String {
        val (salt, pass) = getPBKDF2Password()
        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        cipher.init(Cipher.ENCRYPT_MODE, pass, GCMParameterSpec(128, iv))
        return Base64.getEncoder().encodeToString(salt + iv + cipher.doFinal(plaintext.toByteArray()))
    }

    fun create(user: User): String {
        val moshi = Moshi.Builder().build()
        val userData = mapOf(
            "identifier" to user.identifier,
            "first_name" to user.firstName,
            "last_name" to user.lastName,
            "email" to user.email
        )
        val userInfo = mapOf(
            "timestamp" to (Instant.now().toEpochMilli() / 1000),
            "user" to userData
        )
        val adapter = moshi.adapter(Map::class.java)
        val token = encrypt(adapter.toJson(userInfo))
        val userInfoParam = mapOf(
            "client_id" to clientId,
            "token" to token
        )
        return Base64.getUrlEncoder().encodeToString(adapter.toJson(userInfoParam).toByteArray());
    }

    private fun getPBKDF2Password(): Pair<ByteArray, SecretKey> {
        val skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")
        val spec = PBEKeySpec(secret.toCharArray(), salt, 10_000, 128)
        val key = skf.generateSecret(spec)
        return salt to SecretKeySpec(key.encoded, "AES")
    }

    companion object {
        fun encrypt(clientId: String, secret: String, plaintext: String): String {
            return UserInfoCreator(clientId, secret).encrypt(plaintext)
        }

        fun create(clientId: String, secret: String, user: User): String {
            return UserInfoCreator(clientId, secret).create(user)
        }
    }
}

fun main() {
    val clientId = "your_provided_client_id"
    val clientSecret = "your_provided_client_secret"
    val user = User().apply {
        identifier = "some-client-owned-user-id"
        firstName = "Example"
        lastName = "User"
        email = "example.user@test.local"
    }
    println(UserInfoCreator.create(clientId, clientSecret, user))
}

