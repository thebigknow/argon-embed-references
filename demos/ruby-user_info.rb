# frozen_string_literal: true

require 'json'
require 'securerandom'
require 'openssl'
require 'base64'

client_id = "your_provided_client_id"

user_info = {
  timestamp: Time.now.to_i,
  user:      {
    identifier: 'some-client-owned-user-id',
    first_name: 'Example',
    last_name:  'User',
    email:      'example.user@test.local'
  }
}

secret = "your_provided_client_secret"
salt = SecureRandom.bytes(16)
cipher = OpenSSL::Cipher.new("aes-128-gcm")
cipher.encrypt
key = OpenSSL::KDF.pbkdf2_hmac secret, salt: salt, length: 16, iterations: 10_000, hash: "SHA256"
cipher.key = key
iv = SecureRandom.bytes(12)
cipher.iv = iv

# For GCM append the auth_tag
encrypted = (cipher.update(user_info.to_json) + cipher.final + cipher.auth_tag)

token = Base64.encode64(salt + iv + encrypted)

user_info_param = {
  client_id: client_id,
  token: token
}

puts Base64.urlsafe_encode64 user_info_param.to_json
