import pdb
import sys
import base64
import hashlib
import json
# This must be the PyCryptodome library. PyCrypto does not support GCM
from Crypto.Cipher import AES

if len(sys.argv) != 2:
    print("You must pass the Base64 encoded string as an argument.")
    sys.exit()

client_id = "your_provided_client_id"
secret = b"your_provided_client_secret"

user_info = json.loads(base64.urlsafe_b64decode(sys.argv[1]))

if user_info['client_id'] != client_id:
    raise Exception("ClientId mismatch")

data = base64.b64decode(user_info['token'])
salt = data[0:16]
iv = data[16:28]
encrypted = data[28:-16]
auth_tag = data[-16:]

print("SALT: " + base64.b64encode(salt).decode())
print("IV: " + base64.b64encode(iv).decode())
print("AUTH: " + base64.b64encode(auth_tag).decode())
key_len = 16
iterations = 10000
key = hashlib.pbkdf2_hmac('sha256', secret, salt, iterations, key_len)
cipher = AES.new(key, AES.MODE_GCM, iv)

txt = cipher.decrypt_and_verify(encrypted, auth_tag)
print(txt)
