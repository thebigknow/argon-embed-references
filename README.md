﻿# Authentication for TBK Embeds

The Big Know supports authentication for embeds through an API User requesting access for a specific User. The request returns an access_token that can be inserted before embeds are initialized. In doing so the embed behavior will be very much like the full Lesson experience and activities will be tracked for the given User. Without authentication user activity is only stored in browser localStorage.


Authentication can happen one of two ways; via direct calls to the API or by symmetrically encrypting a user_info packet with an API User's client_secret. Both ways support creating users but that permission must be enabled by TBK for your Academy. I will detail the use of each method below.

## Authentication via the API

There are two methods to get an access_token for the user via the API. If you know the user exists you need only supply their unique identifier and we will return their jwt / access_token with an expiration that matches the Academy's expiration. The request will look like this:


```json
# GET http://api.thebigknow.com/api/public/v1/users/{identifier}
# RESPONSE
{
 "identifier": "the-identifier",
 "first_name": "First",
 "last_name": "Last",
 "email": "email@local",
 "jwt": "the_access_token"
}
```
<a name="fig1">Fig. 1</a>


If you do not know if the user exists and your Academy has access to create users you can use a PUT with the required user attributes instead. If the User does not exist it will be created, otherwise it returns the information for the user it found along with the access_token. For example:

```json
# PUT http://api.thebigknow.com/api/public/v1/users/{identifier}
# REQUEST BODY
{
 "identifier": "the-identifier",
 "first_name": "First",
 "last_name": "Last",
 "email": "email@local"
}

# RESPONSE
{
 "identifier": "the-identifier",
 "first_name": "First",
 "last_name": "Last",
 "email": "email@local",
 "jwt": "the_access_token"
}
```
<a name="fig2">Fig. 2</a>

Once you have the jwt or from either of these methods you can use that when crafting the embed code like this:

```html
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1">
<script src="https://api.uat02.thebigknow.com/api/embed/v1/loader.js?partner=your-subdomain&jwt=the_access_token" async></script>
</head>
<body>
<div class="container">
	<tbk-embed type="activity" identifier="self-assessment-01"></tbk-embed>
</div>
</body>
</html>
```
<a name="fig3">Fig. 3</a>


There are a few important parameters that I will highlight. The first, **partner**, is so we know which Academy (tenant) we are embedding for. Then **jwt** is the token you retrieved for the individual user so we can authenticate them when loading the embeds.

## Authentication via a user_info packet

If you are not already using our API we can simplify the interactions a by foregoing the extra network roundtrip to create the User’s access token by instead creating a symmetrically encrypted user_info packet. You still need an API User to build this because the symmetric key is based on the client_secret and you must be scoped to create Users for a specific Academy. We have reference code to build the encrypted packet in Ruby, Python and Kotlin/Java but I will give technical details below. The high-level of how this works is:

* Prepare the user data (exactly the same as the request body in [fig. 2](#fig2))
* Wrap that in an object that includes a `timestamp` that is the current EPOCH time in seconds
* Encrypt the user data
* Wrap the token with the `client_id`
* Craft the embed code with the encrypted user_info

The resulting embed code will look something like this:

```html
<html>
<head>
 <meta name="viewport" content="width=device-width,initial-scale=1">
 <script src="https://api.uat02.thebigknow.com/api/embed/v1/loader.js?partner=your-subdomain&user_info=the_user_info_packet" async></script>
</head>
<body>
 <div class="container">
   <tbk-embed type="activity" identifier="self-assessment-01"></tbk-embed>
 </div>
</body>
</html>
```
<a name="fig4">Fig. 4</a>

Again, I’ll highlight the important parameters that you will need to pass. The **partner** as before, and the **user_info** packet.

If you are using the reference code you don’t need to know much more about how this works but if you would like to implement the encryption yourself you are welcome to do so. The general steps are as follows:

* Create the User object
* Wrap the User object with a timestamp
* Encode the User object as JSON in the format:
```json
{
  "timestamp": 1607880942,
  {
  "identifier": "the-identifier",
  "first_name": "First",
  "last_name": "Last",
  "email": "email@local"
  }
}
```
* Generate a symmetric key based on the client_secret using a “Password-Based Key Derivation Function” or PBKDF2.
* Use the generated key as an input with the User JSON into an encryption function that uses AES128 + GCM + NoPadding
* Construct the embed code by Base64 encoding (URL safe) the encrypted output

### Password-Based Key Derivation Function

This creates a symmetric key based on the client_secret. Our particular implementation requires HMAC for the pseudo-random function and SHA256 for the digest function. In the various languages we have reference implementations for it looks like this:

* Ruby: `OpenSSL::KDF.pbkdf2_hmac(hash: "SHA256", ...)`
* Java: `SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")`
* Python: `hashlib.pbkdf2_hmac('sha256', ...)`

Other inputs into the PBKDF2 function are a 16 byte salt, an iteration count of 10,000 and an output key length of 16 bytes. It is important to keep the salt to send with the encrypted data.

### Encryption Function

To encrypt the data we use the aforementioned PBKDF2 key, a random 12 byte initialization vector (IV)(Python calls this the NONCE) and a cipher implementing AES128+GCM. Again, this is how it looks in our reference implementations:

* Ruby: `OpenSSL::Cipher.new("aes-128-gcm")`
* Java: `javax.crypto.Cipher.getInstance("AES/GCM/NoPadding")`
* Python: `AES.new(key, AES.MODE_GCM, iv)`  (from PyCryptodome)

Make sure and keep track of the IV because we must send it along with the user_info packet.

### Base64 Encode the user_info packet to create the "token"

Along with the encrypted packet you need to Base64 encode the salt and the IV.

* Ruby: `Base64.encode64(salt + iv + e_data)`
* Java: `Base64.getEncoder().encodeToString(salt + iv + e_data)`
* Python: `base64.b64encode(salt + iv + e_data + auth_tag)`

### Wrap the token with the client_id

As a final step we will wrap the previously generated token with the client_id into a structure that looks like this:

```json
{
  "client_id": "the-client-id",
  "token": "the-base64-output-from-the-previous-step"
}
```

Finally, Base64 encode that data with a URL-safe encoder because we will be adding this as an HTTP query parameter.

* Ruby: `Base64.urlsafe_encode64(wrapped_client_id_json)`
* Java: `Base64.getUrlEncoder().encodeToString(wrapped_client_id_json)`
* Python: `base64.urlsafe_b64encode(wrapped_client_id_json)`

After you’ve done that you should be all set. Craft the embed code like shown in [Fig. 4](#fig4) and you will be up and running with authenticated embeds.

## Reference Implementations

* [Ruby](demos/ruby-user_info.rb)
* [Python](demos/python-user_info.py)
* [Java / Kotlin](demos/kotlin-UserInfoCreator.kt)

Along with the reference encoding demos there is also a [decoder in Ruby](demos/ruby-user_info-decoder.rb) that you can test decoding with.
